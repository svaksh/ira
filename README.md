# IRA
My collection of scientific programming snippets as IPython notebooks with notes. 
View them online via [nbviewer](http://nbviewer.ipython.org/github/svaksha/ira/). 


## NoteBook list
- Dataviz
   - [Introduction](http://nbviewer.ipython.org/github/svaksha/ira/blob/master/2014-dataviz/01_DV_IML_intro.ipynb)
   - [EOL lifecycle](http://nbviewer.ipython.org/github/svaksha/ira/blob/master/2014-dataviz/02_DV_EOL_lifecycle.ipynb)
   - [EOL plots](http://nbviewer.ipython.org/github/svaksha/ira/blob/master/2014-dataviz/03_DV_EOL_plots.ipynb)
- Biology
   - [Transcription Introduction](http://nbviewer.ipython.org/github/svaksha/ira/blob/master/2014-transcriptome/01_transcriptome_intro.ipynb)


# LICENSE
- COPYRIGHT © 2013-Now [SVAKSHA](http://svaksha.com/pages/Bio) All Rights Reserved. 
- This software is distributed under the [AGPLv3 License](http://www.gnu.org/licenses/agpl.html) as listed in the LICENSE.md file and all software copies must retain the Copyright, Licence and this permission notice.

### Mirrors
- [GitLab](https://gitlab.com/svaksha/ira):: git clone git@gitlab.com:svaksha/ira.git
- [Devlabs](https://gitlab.devlabs.linuxassist.net/svaksha/ira):: ssh://git@gitlab.devlabs.linuxassist.net:608/svaksha/ira.git
